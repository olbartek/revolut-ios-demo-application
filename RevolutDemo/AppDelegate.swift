import UIKit

@UIApplicationMain
internal class AppDelegate: UIResponder, UIApplicationDelegate {
    internal var window: UIWindow?

    internal func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootViewController = createRootViewController()
        window?.rootViewController = rootViewController
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    // MARK: - Assembly
    private func createRootViewController() -> UIViewController {
        let defaultEnvironment = Environment(name: "default",
                                             host: "https://revolut.duckdns.org")
        let api = APIClient(environment: defaultEnvironment)
        let repository = AppRepository(api: api)
        let appBuidler = AppBuilder(repository: repository)
        let rootViewController = appBuidler.buildCurrenciesConverter()
        
        return rootViewController
    }
}

