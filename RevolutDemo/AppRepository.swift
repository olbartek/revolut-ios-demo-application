internal class AppRepository: CurrenciesConverterEntityGateway {
    private let api: APIClient
    
    internal init(api: APIClient) {
        self.api = api
    }
    
    internal func getCurrenciesRates(baseCurrencyCode: CurrencyCode,
                                     success: @escaping ([CurrencyRate]) -> (),
                                     failure: @escaping (Error) -> ()) -> Cancellable {
        let request = CurrenciesRates(baseCurrency: baseCurrencyCode)
        return api.currenciesRates(request: request) { result in
            switch result {
            case .success(let result):
                let adapter = APICurrencyRatesAdapter(apiResponse: result)
                do {
                    let currencyRates = try adapter.toCurrencyRates()
                    success(currencyRates)
                } catch {
                    failure(error)
                }
            case .error(let error):
                failure(error)
            }
        }
    }
}
