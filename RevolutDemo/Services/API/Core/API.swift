import Foundation

public protocol API: class {
    var session: URLSession { get }
    var requestBuilder: RequestBuilding { get }
    var errorResponseParser: ErrorResponseParsing { get }
}

public extension API {
    @discardableResult
    func execute<Req: Request>(request: Req,
                               completion: @escaping (APIResult<Req.Response>) -> Void) -> Cancellable {
        do {
            let urlRequest = try requestBuilder.buildURLRequest(from: request)
            let dataTask = session.dataTask(with: urlRequest) { [weak self] (data, response, error) in
                guard let self = self else { return }
                
                let result: APIResult<Req.Response> = self.errorResponseParser
                    .parse(data: data, urlResponse: response, error: error)
                
                DispatchQueue.main.async {
                    completion(result)
                }
                
            }
            dataTask.resume()
            
            return dataTask
        } catch {
            let errorResponse: APIResult<Req.Response> = .error(.requestAdaptingFailure(error))
            DispatchQueue.main.async {
                completion(errorResponse)
            }
            
            return EmptyCancellable()
        }
    }
}

extension URLSessionDataTask: Cancellable {}

private struct EmptyCancellable: Cancellable {
    func cancel() {}
}
