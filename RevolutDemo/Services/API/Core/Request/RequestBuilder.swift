import Foundation

internal class RequestBuilder: RequestBuilding {
    internal let environment: Environment
    internal let jsonEncoder: JSONEncoding
    
    internal init(environment: Environment,
                  jsonEncoder: JSONEncoding = JSONEncoder()) {
        self.environment = environment
        self.jsonEncoder = jsonEncoder
    }
    
    internal func buildURLRequest<Req>(from request: Req) throws -> URLRequest where Req: Request {
        let urlString = environment.host + "/" + request.path
        guard let url = URL(string: urlString) else {
            throw RequestBuilderError.invalidUrl
        }
        
        let httpBody = try jsonEncoder.encode(request)
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.addValue("application/json",
                            forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json",
                            forHTTPHeaderField: "Accept")
        urlRequest.httpBody = httpBody
        
        return urlRequest
    }
}

internal enum RequestBuilderError: Error {
    case invalidUrl
}
