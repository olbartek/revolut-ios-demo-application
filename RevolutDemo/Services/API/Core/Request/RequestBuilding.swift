import Foundation

public protocol RequestBuilding {
    func buildURLRequest<Req: Request>(from request: Req) throws -> URLRequest
}
