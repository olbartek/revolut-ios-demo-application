@testable import RevolutDemo
import XCTest

internal class RequestBuilderTests: XCTestCase {
    internal var jsonEncoder: MockJSONEncoder!
    internal var requestBuilder: RequestBuilding!
    
    override internal func setUp() {
        super.setUp()
        jsonEncoder = MockJSONEncoder()
    }
    
    override internal func tearDown() {
        jsonEncoder = nil
        super.tearDown()
    }
    
    internal func testRequestBuilderShouldThrowOnInvalidURL() throws {
        // Arrange
        jsonEncoder.shouldThrow = false
        let environment = Environment(name: "fixture",
                                      host: "fix ture")
        requestBuilder = RequestBuilder(environment: environment,
                                        jsonEncoder: jsonEncoder)
        let request = MockRequest(value: "fixture")
        
        // Act & Assert
        XCTAssertThrowsError(try requestBuilder.buildURLRequest(from: request))
    }
    
    internal func testRequestBuilderShouldThrowOnJSONEncodingError() throws {
        // Arrange
        jsonEncoder.shouldThrow = true
        let environment = Environment(name: "fixture",
                                      host: "http://www.example.com")
        requestBuilder = RequestBuilder(environment: environment,
                                        jsonEncoder: jsonEncoder)
        let request = MockRequest(value: "fixture")
        
        // Act & Assert
        XCTAssertThrowsError(try requestBuilder.buildURLRequest(from: request))
    }
    
    internal struct MockRequest: Request {
        internal var path: String {
            return String(format: "mock/%@", value)
        }
        public let method: HTTPMethod = .GET
        
        public let value: String
        
        public init(value: String) {
            self.value = value
        }
        
        public struct Response: Decodable {
            let value: String
        }
        
        private enum CodingKeys: CodingKey {}
    }

    internal class MockJSONEncoder: JSONEncoding {
        internal var shouldThrow = false
        
        internal func encode<T>(_ value: T) throws -> Data where T : Encodable {
            if shouldThrow {
                throw MockError.fixture
            }
            
            return Data()
        }
    }
    
    private enum MockError: Error {
        case fixture
    }
}
