public enum HTTPMethod: String, Codable {
    case GET, POST, PUT, PATCH, DELETE
}
