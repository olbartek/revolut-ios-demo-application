import Foundation

public class APIClient: API {
    public let session: URLSession
    public let requestBuilder: RequestBuilding
    public let errorResponseParser: ErrorResponseParsing
    
    public init(environment: Environment) {
        self.session = URLSession(configuration: .default)
        self.requestBuilder = RequestBuilder(environment: environment)
        self.errorResponseParser = ErrorResponseParser()
    }
    
    public func currenciesRates(request: CurrenciesRates,
                                completion: @escaping APIResultHandler<CurrenciesRates.Response>) -> Cancellable {
        return execute(request: request, completion: completion)
    }
}
