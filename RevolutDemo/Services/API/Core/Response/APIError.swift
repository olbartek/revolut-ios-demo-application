public enum APIError: Error {
    case requestAdaptingFailure(Error)
    case serializationFailure
    
    case noData
    case wrongHTTPCode(code: Int)
    case unknownError(Error)
}
