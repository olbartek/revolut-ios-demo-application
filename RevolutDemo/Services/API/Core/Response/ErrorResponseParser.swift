import Foundation

public class ErrorResponseParser: ErrorResponseParsing {
    private let jsonDecoder: JSONDecoding
    
    public init(jsonDecoder: JSONDecoding = JSONDecoder()) {
        self.jsonDecoder = jsonDecoder
    }
    
    public func parse<T: Decodable>(data: Data?,
                                    urlResponse: URLResponse?,
                                    error: Error?) -> APIResult<T> {
        
        if let error = error {
            return .error(.unknownError(error))
        }
        
        guard let data = data else {
            return .error(.noData)
        }
        
        guard let httpUrlResponse = urlResponse as? HTTPURLResponse else {
            preconditionFailure("Expected to have HTTPURLResponse")
        }
        
        guard httpUrlResponse.statusCode == 200 else {
            return .error(.wrongHTTPCode(code: httpUrlResponse.statusCode))
        }
        
        do {
            let response = try self.jsonDecoder.decode(T.self, from: data)
            return .success(response)
        } catch {
            return .error(.serializationFailure)
        }
    }
}
