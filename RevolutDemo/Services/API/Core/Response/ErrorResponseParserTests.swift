@testable import RevolutDemo
import XCTest

internal class ErrorResponseParserTests: XCTestCase {
    internal var sut: ErrorResponseParsing!
    
    override internal func setUp() {
        super.setUp()
        sut = ErrorResponseParser()
    }
    
    override internal func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    internal func testResultIsParsedToUnknownErorWhenErrorIsPresentOnInput() {
        // Act
        let result: APIResult<StubDecodable> = sut.parse(data: nil, urlResponse: nil, error: MockError.fixture)
        
        // Assert
        guard case .error(.unknownError(MockError.fixture)) = result else {
            XCTFail()
            return
        }
    }
    
    internal func testResultIsParsedToNoDataErrorWhenThereIsNoDataOnInput() {
        // Act
        let result: APIResult<StubDecodable> = sut.parse(data: nil, urlResponse: nil, error: nil)
        
        // Assert
        guard case .error(.noData) = result else {
            XCTFail()
            return
        }
    }
    
    internal func testResultIsParsedToWrongHttpCodeErrorForCodesDifferentThanTwoHundred() {
        // Arrange
        let urlResponse = HTTPURLResponse(url: URL(string: "http://example.com")!,
                                          statusCode: 300,
                                          httpVersion: nil,
                                          headerFields: nil)
        
        // Act
        let result: APIResult<StubDecodable> = sut.parse(data: Data(),
                                                         urlResponse: urlResponse,
                                                         error: nil)
        
        // Assert
        guard case .error(.wrongHTTPCode) = result else {
            XCTFail()
            return
        }
    }
    
    internal func testResultIsParsedToSerializationFailureErrorWhenWrongJSONDataIsProvided() {
        // Arrange
        let urlResponse = HTTPURLResponse(url: URL(string: "http://example.com")!,
                                          statusCode: 200,
                                          httpVersion: nil,
                                          headerFields: nil)
        let data = convertToData(fromJSON: "fixture")
        
        // Act
        let result: APIResult<StubDecodable> = sut.parse(data: data,
                                                         urlResponse: urlResponse,
                                                         error: nil)
        
        // Assert
        guard case .error(.serializationFailure) = result else {
            XCTFail()
            return
        }
    }
    
    internal func testResultIsParsedToSuccessWhenValidJSONDataIsProvided() {
        // Arrange
        let urlResponse = HTTPURLResponse(url: URL(string: "http://example.com")!,
                                          statusCode: 200,
                                          httpVersion: nil,
                                          headerFields: nil)
        let data = convertToData(fromJSON: "{ \"valueA\":\"FIXTUREA\", \"valueB\":\"FIXTUREB\" }")
        
        // Act
        let result: APIResult<StubDecodable> = sut.parse(data: data,
                                                         urlResponse: urlResponse,
                                                         error: nil)
        
        // Assert
        guard case .success(let response) = result else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(response.valueA, "FIXTUREA")
        XCTAssertEqual(response.valueB, "FIXTUREB")
    }
    
    // MARK: - Helpers
    private func convertToData(fromJSON json: String) -> Data? {
        return json.data(using: .utf8)
    }
    
    // MARK: - Mocks
    internal struct StubDecodable: Decodable, Equatable {
        internal let valueA: String
        internal let valueB: String
        
        internal init(valueA: String, valueB: String) {
            self.valueA = valueA
            self.valueB = valueB
        }
    }
    
    private enum MockError: Error {
        case fixture
    }
}


