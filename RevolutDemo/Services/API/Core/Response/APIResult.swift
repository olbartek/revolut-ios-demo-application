public enum APIResult<T> {
    case success(T)
    case error(APIError)
}

public typealias APIResultHandler<T> = (APIResult<T>) -> Void
