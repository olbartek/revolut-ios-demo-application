import Foundation

public protocol ErrorResponseParsing {
    func parse<T:Decodable>(data: Data?, urlResponse: URLResponse?, error: Error?) -> APIResult<T>
}
