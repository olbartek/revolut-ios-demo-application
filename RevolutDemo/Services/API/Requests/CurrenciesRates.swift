import Foundation

public struct CurrenciesRates: Request {
    public var path: String {
        return String(format: "latest?base=%@", baseCurrency)
    }
    public let method: HTTPMethod = .GET
    
    public let baseCurrency: String
    
    public init(baseCurrency: String) {
        self.baseCurrency = baseCurrency
    }
    
    public struct Response: Decodable {
        public typealias CurrencyCode = String
        public typealias Rate = Decimal
        
        public let base: CurrencyCode
        public let rates: [CurrencyCode: Rate]
    }
    
    private enum CodingKeys: CodingKey {}
}
