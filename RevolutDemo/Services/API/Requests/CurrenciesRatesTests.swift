@testable import RevolutDemo
import XCTest

internal class CurrenciesRatesTests: XCTestCase {
    private let environment = Environment(name: "fixture", host: "http://www.example.com")
    
    internal func testRequestConstruction() throws {
        // Arrange
        let requestBuilder = RequestBuilder(environment: environment)
        let request = CurrenciesRates(baseCurrency: "fixture")
        
        // Act
        let urlRequest = try requestBuilder.buildURLRequest(from: request)
        
        // Assert
        XCTAssertEqual(urlRequest.url?.absoluteString, "http://www.example.com/latest?base=fixture")
        XCTAssertEqual(String(data: urlRequest.httpBody!, encoding: .utf8), "{}")
        XCTAssertEqual(urlRequest.httpMethod, HTTPMethod.GET.rawValue)
    }
}
