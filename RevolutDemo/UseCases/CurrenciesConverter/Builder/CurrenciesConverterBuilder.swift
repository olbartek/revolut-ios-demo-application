import UIKit

public protocol CurrenciesConverterBuilder: class {
    func buildCurrenciesConverter() -> UIViewController
}
