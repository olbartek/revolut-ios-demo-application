import Foundation

public struct CurrenciesConverterViewModel: Equatable {
    public var currencies: [CurrencyViewModel] = []
    public var isKeyboardHidden: Bool = true
}

public struct CurrencyViewModel: Hashable {
    public let code: String
    public let name: String
    public let rate: Decimal
    public let amount: String
    private(set) public var isFocused: Bool
    
    public init(code: String, name: String, rate: Decimal, amount: String, isFocused: Bool) {
        self.code = code
        self.name = name
        self.rate = rate
        self.amount = amount
        self.isFocused = isFocused
    }
    
    public mutating func setFocus(_ isFocused: Bool) {
        self.isFocused = isFocused
    }
}
