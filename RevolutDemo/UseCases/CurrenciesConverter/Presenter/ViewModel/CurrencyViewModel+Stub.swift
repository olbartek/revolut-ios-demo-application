import Foundation

internal extension CurrencyViewModel {
    static func stub(code: String = "fixture",
                     name: String = "fixture",
                     isFocused: Bool = false,
                     rate: Decimal = 1.0,
                     amount: String = "1.0") -> CurrencyViewModel {
        return CurrencyViewModel(code: code,
                                 name: name,
                                 rate: rate,
                                 amount: amount,
                                 isFocused: isFocused)
    }
}
