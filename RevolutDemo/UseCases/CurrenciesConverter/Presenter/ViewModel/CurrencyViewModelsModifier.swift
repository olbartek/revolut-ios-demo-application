import Foundation

public class CurrencyViewModelsModifier {
    public let currencies: [CurrencyViewModel]
    
    public init(currencies: [CurrencyViewModel]) {
        self.currencies = currencies
    }
    
    public func setFocus(_ isFocused: Bool,
                         onRowAtIndex index: Int) -> CurrencyViewModelsModifier {
        var currencyViewModel = currencies[index]
        currencyViewModel.setFocus(isFocused)
        
        var updatedCurrencies = currencies
        updatedCurrencies.remove(at: index)
        updatedCurrencies.insert(currencyViewModel, at: index)
        
        return CurrencyViewModelsModifier(currencies: updatedCurrencies)
    }
    
    public func moveRowToTop(fromIndex index: Int) -> CurrencyViewModelsModifier {
        let viewModelToBeMoved = currencies[index]
        var updatedCurrencies = currencies
        updatedCurrencies.remove(at: index)
        updatedCurrencies.insert(viewModelToBeMoved, at: 0)
        
        return CurrencyViewModelsModifier(currencies: updatedCurrencies)
    }
    
    public func updateRates(with currencyRates: [CurrencyRate],
                            multipliedBy baseAmount: Decimal,
                            decimalToStringConverter: (Decimal) -> String,
                            currentStringAmount: String) -> CurrencyViewModelsModifier {
        var updatedCurrencies = currencies
        
        currencyRates.forEach { currencyRate in
            let amount = currencyRate.rate * baseAmount
            let idx = index(of: currencyRate.toCurrency.code)
            let textAmount = idx == 0 ? currentStringAmount : decimalToStringConverter(amount)
            let updatedViewModel = CurrencyViewModel(
                code: currencyRate.toCurrency.code,
                name: currencyRate.toCurrency.name,
                rate: currencyRate.rate,
                amount: textAmount,
                isFocused: false
            )
            
            if let index = idx {
                updatedCurrencies[index] = updatedViewModel
            } else {
                updatedCurrencies.append(updatedViewModel)
            }
        }
        
        return CurrencyViewModelsModifier(currencies: updatedCurrencies)
    }
    
    public func updateRates(multipliedBy baseAmount: Decimal,
                            decimalToStringConverter: (Decimal) -> String,
                            currentStringAmount: String) -> CurrencyViewModelsModifier {
        
        let updatedCurrencies: [CurrencyViewModel] = currencies.enumerated()
            .map { index, currencyViewModel in
                
                let amount = currencyViewModel.rate * baseAmount
                let textAmount = index == 0 ? currentStringAmount : decimalToStringConverter(amount)
                
                return CurrencyViewModel(code: currencyViewModel.code,
                                         name: currencyViewModel.name,
                                         rate: currencyViewModel.rate,
                                         amount: textAmount,
                                         isFocused: false)
        }
        
        return CurrencyViewModelsModifier(currencies: updatedCurrencies)
    }
    
    private func index(of currencyCode: CurrencyCode) -> Int? {
        return currencies.firstIndex(where: { $0.code == currencyCode })
    }
}
