@testable import RevolutDemo
import XCTest

internal class CurrencyViewModelsModifierTests: XCTestCase {
    internal var sut: CurrencyViewModelsModifier!
    
    override internal func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    internal func testSettingFocus() {
        // Arrange
        let currencies: [CurrencyViewModel] = [
            .stub(),
            .stub(),
            .stub()
        ]
        sut = CurrencyViewModelsModifier(currencies: currencies)
        
        // Act
        let updatedCurrencies = sut
            .setFocus(true, onRowAtIndex: 0)
            .setFocus(true, onRowAtIndex: 2)
            .currencies
        
        // Assert
        XCTAssertEqual(currencies.map { $0.isFocused }, [false, false, false])
        XCTAssertEqual(updatedCurrencies.map { $0.isFocused }, [true, false, true])
    }
    
    internal func testMovingRowToTop() {
        // Arrange
        let currencies: [CurrencyViewModel] = [
            .stub(code: "CurrencyA"),
            .stub(code: "CurrencyB"),
            .stub(code: "CurrencyC")
        ]
        sut = CurrencyViewModelsModifier(currencies: currencies)
        
        // Act
        let updatedCurrencies = sut
            .moveRowToTop(fromIndex: 1)
            .currencies
        
        // Assert
        XCTAssertEqual(currencies[0], updatedCurrencies[1])
        XCTAssertEqual(currencies[1], updatedCurrencies[0])
        XCTAssertEqual(currencies[2], updatedCurrencies[2])
    }
    
    internal func testUpdatingRatesWithCurrencyRates() {
        // Arrange
        let currencies: [CurrencyViewModel] = [
            .stub(code: "CurrencyA", amount: "10"),
            .stub(code: "CurrencyB", amount: "20"),
            .stub(code: "CurrencyC", amount: "30")
        ]
        sut = CurrencyViewModelsModifier(currencies: currencies)
        let currencyRates: [CurrencyRate] = [
            CurrencyRate(fromCurrency: .stub(code: "CurrencyA"),
                         toCurrency: .stub(code: "CurrencyA"),
                         rate: 1),
            CurrencyRate(fromCurrency: .stub(code: "CurrencyA"),
                         toCurrency: .stub(code: "CurrencyC"),
                         rate: 3),
            CurrencyRate(fromCurrency: .stub(code: "CurrencyA"),
                         toCurrency: .stub(code: "CurrencyB"),
                         rate: 4)
        ]
        
        // Act
        let updatedCurrencies = sut
            .updateRates(with: currencyRates,
                         multipliedBy: 10,
                         decimalToStringConverter: { decimal in return "" },
                         currentStringAmount: "100")
            .currencies
        
        // Assert
        XCTAssertEqual(updatedCurrencies[0].code, currencies[0].code)
        XCTAssertEqual(updatedCurrencies[1].code, currencies[1].code)
        XCTAssertEqual(updatedCurrencies[2].code, currencies[2].code)
        
        XCTAssertEqual(updatedCurrencies[0].rate, currencyRates[0].rate)
        XCTAssertEqual(updatedCurrencies[1].rate, currencyRates[2].rate)
        XCTAssertEqual(updatedCurrencies[2].rate, currencyRates[1].rate)
        
        XCTAssertEqual(updatedCurrencies[0].amount, "100")
        XCTAssertTrue(updatedCurrencies[1].amount != currencies[2].amount)
        XCTAssertTrue(updatedCurrencies[2].amount != currencies[1].amount)
        
        XCTAssertFalse(updatedCurrencies[0].isFocused)
        XCTAssertFalse(updatedCurrencies[1].isFocused)
        XCTAssertFalse(updatedCurrencies[2].isFocused)
    }
    
    internal func testUpdatingRatesWithNewStringInput() {
        // Arrange
        let currencies: [CurrencyViewModel] = [
            .stub(code: "CurrencyA", rate: 1, amount: "10"),
            .stub(code: "CurrencyB", rate: 2, amount: "20"),
            .stub(code: "CurrencyC", rate: 3, amount: "30")
        ]
        sut = CurrencyViewModelsModifier(currencies: currencies)
        
        // Act
        let updatedCurrencies = sut
            .updateRates(multipliedBy: 10,
                         decimalToStringConverter: { decimal in return "" },
                         currentStringAmount: "100")
            .currencies
        
        // Assert
        XCTAssertEqual(updatedCurrencies[0].code, currencies[0].code)
        XCTAssertEqual(updatedCurrencies[1].code, currencies[1].code)
        XCTAssertEqual(updatedCurrencies[2].code, currencies[2].code)
        
        XCTAssertEqual(updatedCurrencies[0].rate, currencies[0].rate)
        XCTAssertEqual(updatedCurrencies[1].rate, currencies[1].rate)
        XCTAssertEqual(updatedCurrencies[2].rate, currencies[2].rate)
        
        XCTAssertEqual(updatedCurrencies[0].amount, "100")
        XCTAssertTrue(updatedCurrencies[1].amount != currencies[1].amount)
        XCTAssertTrue(updatedCurrencies[2].amount != currencies[2].amount)
    }

}
