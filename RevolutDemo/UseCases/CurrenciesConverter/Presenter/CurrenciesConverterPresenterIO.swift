import DeepDiff

public protocol CurrenciesConverterView: class {
    func reload(changes: [Change<CurrencyViewModel>], updateData: () -> Void)
    func reload(rows: [Int])
    func scrollToFirstRowIfNeeded()
    func clearFocus()
}

public protocol CurrenciesConverterPresentation: class {
    var viewModel: CurrenciesConverterViewModel { get }
}

public protocol CurrenciesConverterEventHandler: class {
    func handleViewReady()
    func handleDidSelectRow(atIndex index: Int)
    func handleWillStartDraggingTableView()
    func handleTextFieldTextShouldChange(to newText: String) -> Bool
}
