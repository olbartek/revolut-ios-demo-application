import DeepDiff
import Foundation

public final class CurrenciesConverterPresenter: CurrenciesConverterPresentation,
    DecimalAmountConverting, DecimalInputAllowing {
    internal typealias Dispatcher = (@escaping () -> Void) -> Void
    
    // MARK: - Properties
    public weak var view: CurrenciesConverterView!
    public var interactor: CurrenciesConverterUseCase!
    public var viewModel: CurrenciesConverterViewModel
    
    public var updateTimer: UpdateTimerInterface
    public let locale = Locale.autoupdatingCurrent
    public let maximumNumberOfFractionalPartDigits: Int = 2
    public let maximumNumberOfIntegralPartDigits: Int = 9
    internal var delayDispatcher: Dispatcher = { closure in
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100),
                                      execute: closure)
    }
    
    // INFO: Use some default parameters as a starting point for now
    //       Normally, the application could persist those values, so the users
    //       could start from the same values when they launch the app again
    internal var currentBaseAmount: Decimal = 100.0
    internal var currentBaseStringAmount: String = ""

    // MARK: - Initializers
    public init(updateTimer: UpdateTimerInterface = UpdateTimer(timeInterval: 1.0)) {
        self.updateTimer = updateTimer
        self.viewModel = CurrenciesConverterViewModel()
        self.currentBaseStringAmount = convertToStringAmount(value: currentBaseAmount)
    }
    
    // MARK: - Private methods
    private func startFetchingCurrencies(baseCurrencyCode: CurrencyCode) {
        updateTimer.stopUpdates()
        updateTimer.startUpdates { [weak interactor] in
            interactor?.cancelFetchingCurrenciesRates()
            interactor?.fetchCurrenciesRates(baseCurrencyCode: baseCurrencyCode)
        }
    }
    
    private func updateViewModel(with currencies: [CurrencyViewModel]) {
        let changes = diff(old: viewModel.currencies,
                           new: currencies,
                           algorithm: WagnerFischer(reduceMove: true))
        view.reload(changes: changes, updateData: {
            viewModel.currencies = currencies
        })
    }
}

extension CurrenciesConverterPresenter: CurrenciesConverterEventHandler {
    public func handleViewReady() {
        startFetchingCurrencies(baseCurrencyCode: "EUR")
    }
    
    public func handleDidSelectRow(atIndex index: Int) {
        viewModel.isKeyboardHidden = false
        
        let focusModifier = CurrencyViewModelsModifier(currencies: viewModel.currencies)
        viewModel.currencies = focusModifier
            .setFocus(true, onRowAtIndex: index)
            .setFocus(false, onRowAtIndex: 0)
            .currencies
        
        view.reload(rows: [0, index])
        
        let modifier = CurrencyViewModelsModifier(currencies: viewModel.currencies)
        let newCurrencies = modifier
            .moveRowToTop(fromIndex: index)
            .currencies
        updateViewModel(with: newCurrencies)
        delayDispatcher {
            self.view.scrollToFirstRowIfNeeded()
        }
        
        let movedViewModel = newCurrencies[0]
        guard let currentAmount = Decimal(string: movedViewModel.amount, locale: locale) else {
            assertionFailure("Expected to have convertible decimal amount")
            return
        }
        
        currentBaseAmount = currentAmount
        currentBaseStringAmount = movedViewModel.amount
        
        guard index != 0 else {
            return
        }
        
        startFetchingCurrencies(baseCurrencyCode: movedViewModel.code)
    }
    
    public func handleWillStartDraggingTableView() {
        guard !viewModel.isKeyboardHidden else {
            return
        }
        
        viewModel.isKeyboardHidden = true
        
        let modifier = CurrencyViewModelsModifier(currencies: viewModel.currencies)
        viewModel.currencies = modifier.setFocus(false, onRowAtIndex: 0).currencies
        view.reload(rows: [0])
        view.clearFocus()
    }
    
    public func handleTextFieldTextShouldChange(to newText: String) -> Bool {
        guard shouldAllow(decimalInput: newText,
                          currentDecimalString: currentBaseStringAmount) else {
            return false
        }
        
        let updatedText = newText
            .zeroStringIfEmpty()
            .toUnsignedIntegerStringIfPossible()
        
        guard let currentAmount = Decimal(string: updatedText, locale: locale) else {
            return false
        }
        
        currentBaseAmount = currentAmount
        currentBaseStringAmount = updatedText
        
        let isKeyboardHidden = viewModel.isKeyboardHidden
        let modifier = CurrencyViewModelsModifier(currencies: viewModel.currencies)
        let updatedCurrencies = modifier
            .updateRates(
                multipliedBy: currentBaseAmount,
                decimalToStringConverter: convertToStringAmount(value:),
                currentStringAmount: currentBaseStringAmount)
            .setFocus(!isKeyboardHidden, onRowAtIndex: 0)
            .currencies
        
        updateViewModel(with: updatedCurrencies)
        
        return false
    }
}

extension CurrenciesConverterPresenter: CurrenciesConverterUseCaseOutput {
    public func didFetchCurrenciesRates(response: [CurrencyRate]) {
        let isKeyboardHidden = viewModel.isKeyboardHidden
        let modifier = CurrencyViewModelsModifier(currencies: viewModel.currencies)
        
        let updatedCurrencies = modifier
            .updateRates(
                with: response,
                multipliedBy: currentBaseAmount,
                decimalToStringConverter: convertToStringAmount(value:),
                currentStringAmount: currentBaseStringAmount)
            .setFocus(!isKeyboardHidden, onRowAtIndex: 0)
        .currencies
        
        updateViewModel(with: updatedCurrencies)
    }
    
    public func didFailFetchingCurrenciesRates(error: Error) {
        // INFO: Fail silently for now, it's not determined how the application
        //       should behave when fetching currency rates fails
    }
}

