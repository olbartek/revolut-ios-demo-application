import DeepDiff
@testable import RevolutDemo
import XCTest

extension CurrenciesConverterPresenterTests {
    internal func testPresenterCallsTimerStopAndStartUpdatesOnViewReady() {
        // Act
        presenter.handleViewReady()
        
        // Assert
        XCTAssertTrue(timer.stopUpdatesCalled)
        XCTAssertTrue(timer.startUpdatesCalled)
    }
    
    internal func testInteractorTriggeredByTimerCallsFetchCurrenciesgOnViewReady() {
        // Act
        presenter.handleViewReady()
        timer.updateHandler?()
        
        // Assert
        XCTAssertTrue(interactor.fetchCurrenciesRatesCalled)
    }
    
    
    internal func testViewIsReloadedOnDidFetchCurrenciesRates() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
           .stub(),
           .stub(),
           .stub()
        ]
        
        // Act
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Assert
        XCTAssertNotNil(view.reloadedChanges)
    }
    
    internal func testViewIsNotReloadedOnDidFailFetchinghCurrenciesRates() {
        // Act
        presenter.didFailFetchingCurrenciesRates(error: MockError.fixture)
        
        // Assert
        XCTAssertNil(view.reloadedChanges)
    }
    
    internal func testKeyboardIsShownOnRowSelectionWhenItIsHidden() {
        // Arrange
        presenter.viewModel.isKeyboardHidden = true
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 1)
        
        // Assert
        XCTAssertFalse(presenter.viewModel.isKeyboardHidden)
    }
    
    internal func testSelectedRowAndFirstRowIsReloadedOnRowSelection() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 1)
        
        // Assert
        XCTAssertEqual(view.reloadedRows, [0, 1])
        XCTAssertTrue(view.scrollToFirstRowIfNeededCalled)
    }
    
    internal func testRowsAreSwitchedOnRowSelection() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 2)
        
        // Assert
        let change = view.reloadedChanges!.first!
        guard case Change<CurrencyViewModel>.move(let move) = change else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(move.fromIndex, 2)
        XCTAssertEqual(move.toIndex, 0)
    }
    
    internal func testTimerIsStoppedAndStartedAgainWhenNewCurrencyIsSelected() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(toCurrency: .stub(code: "CurrencyA")),
            .stub(toCurrency: .stub(code: "CurrencyB")),
            .stub(toCurrency: .stub(code: "CurrencyC"))
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 1)
        
        // Assert
        XCTAssertTrue(timer.stopUpdatesCalled)
        XCTAssertTrue(timer.startUpdatesCalled)
    }
    
    internal func testInteractorStartsFetchingCurrenciesRatesForSelectedCurrency() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(toCurrency: .stub(code: "CurrencyA")),
            .stub(toCurrency: .stub(code: "CurrencyB")),
            .stub(toCurrency: .stub(code: "CurrencyC"))
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 1)
        timer.updateHandler?()
        
        // Assert
        XCTAssertTrue(interactor.fetchCurrenciesRatesCalled)
        XCTAssertEqual(interactor.fetchCurrenciesRateCurrencyCode, "CurrencyB")
    }
    
    internal func testInteractorDoesNotStartFetchingCurrenciesRatesForTheSameCurrency() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(toCurrency: .stub(code: "CurrencyA")),
            .stub(toCurrency: .stub(code: "CurrencyB")),
            .stub(toCurrency: .stub(code: "CurrencyC"))
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleDidSelectRow(atIndex: 0)
        
        // Assert
        XCTAssertFalse(interactor.fetchCurrenciesRatesCalled)
        XCTAssertNil(interactor.fetchCurrenciesRateCurrencyCode)
    }
    
    internal func testKeyboardIsHiddenOnTableViewDragIfItIsNotHidden() {
        // Arrange
        presenter.viewModel.isKeyboardHidden = false
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleWillStartDraggingTableView()
        
        // Assert
        XCTAssertTrue(presenter.viewModel.isKeyboardHidden)
        XCTAssertEqual(view.reloadedRows, [0])
        XCTAssertTrue(view.clearFocusCalled)
    }
    
    internal func testKeyboardIsNotHiddenOnViewDragWhenItIsAlreadyHidden() {
        // Arrange
        presenter.viewModel.isKeyboardHidden = true
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        presenter.handleWillStartDraggingTableView()
        
        // Assert
        XCTAssertTrue(presenter.viewModel.isKeyboardHidden)
        XCTAssertNil(view.reloadedRows)
        XCTAssertFalse(view.clearFocusCalled)
    }
    
    internal func testPresenterDoesNotReloadViewWhenTheInputIsNotAllowed() {
        // Act
        let shouldChange = presenter.handleTextFieldTextShouldChange(to: "123.45678")
        
        // Assert
        XCTAssertNil(view.reloadedChanges)
        XCTAssertFalse(shouldChange)
    }
    
    internal func testPresenterReloadsViewWhenTheInputIsEmpty() {
        // Arrange
        presenter.viewModel.isKeyboardHidden = true
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        _ = presenter.handleTextFieldTextShouldChange(to: "")
        
        // Assert
        XCTAssertNotNil(view.reloadedChanges)
        XCTAssertEqual(presenter.viewModel.currencies.first!.amount, "0")
    }
    
    internal func testPresenterOmitsLeadingZerosWhenConvertingDecimals() {
        // Arrange
        presenter.viewModel.isKeyboardHidden = true
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        _ = presenter.handleTextFieldTextShouldChange(to: "010")
        
        // Assert
        XCTAssertNotNil(view.reloadedChanges)
        XCTAssertEqual(presenter.viewModel.currencies.first!.amount, "10")
    }
    
    internal func testPresenterDoesNotReloadViewWhenTheInputIsNotADecimal() {
        // Act
        let shouldChange = presenter.handleTextFieldTextShouldChange(to: "hello")
        
        // Assert
        XCTAssertNil(view.reloadedChanges)
        XCTAssertFalse(shouldChange)
    }
    
    internal func testPresenterReloadsViewWhenTheInputIsValid() {
        // Arrange
        let currenciesRates: [CurrencyRate] = [
            .stub(),
            .stub(),
            .stub()
        ]
        presenter.didFetchCurrenciesRates(response: currenciesRates)
        
        // Act
        let shouldChange = presenter.handleTextFieldTextShouldChange(to: "50.66")
        
        // Assert
        XCTAssertNotNil(view.reloadedChanges)
        // INFO: Presenter does not change the textfield's text
        //       as it is reloaded with the new value
        XCTAssertFalse(shouldChange)
    }
}

private enum MockError: Error {
    case fixture
}
