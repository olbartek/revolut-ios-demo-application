import DeepDiff
@testable import RevolutDemo
import XCTest

internal class CurrenciesConverterPresenterTests: XCTestCase {
    internal var presenter: CurrenciesConverterPresenter!
    internal var view: MockView!
    internal var interactor: MockInteractor!
    internal var timer: MockUpdateTimer!

    override internal func setUp() {
        super.setUp()
        
        timer = MockUpdateTimer()
        presenter = CurrenciesConverterPresenter(updateTimer: timer)
        view = MockView()
        interactor = MockInteractor()
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.delayDispatcher = { closure in
            closure()
        }
    }
    
    override internal func tearDown() {
        presenter = nil
        view = nil
        interactor = nil
        timer = nil
        
        super.tearDown()
    }
    
    // MARK: - Mocks
    internal class MockInteractor: CurrenciesConverterUseCase {
        internal var fetchCurrenciesRatesCalled = false
        internal var fetchCurrenciesRateCurrencyCode: CurrencyCode?
        internal var cancelFetchingCurrenciesRateCalled = false
        
        internal func fetchCurrenciesRates(baseCurrencyCode: CurrencyCode) {
            fetchCurrenciesRatesCalled = true
            fetchCurrenciesRateCurrencyCode = baseCurrencyCode
        }
        
        internal func cancelFetchingCurrenciesRates() {
            cancelFetchingCurrenciesRateCalled = true
        }
    }
    
    internal class MockView: CurrenciesConverterView {
        internal var reloadedRows: [Int]?
        internal var reloadedChanges: [Change<CurrencyViewModel>]?
        internal var updateDataHandler: (() -> Void)?
        internal var scrollToFirstRowIfNeededCalled = false
        internal var clearFocusCalled = false
        
        internal func reload(rows: [Int]) {
            reloadedRows = rows
        }
        
        internal func reload(changes: [Change<CurrencyViewModel>], updateData: () -> Void) {
            reloadedChanges = changes
            updateData()
        }
        
        internal func scrollToFirstRowIfNeeded() {
            scrollToFirstRowIfNeededCalled = true
        }
        
        internal func clearFocus() {
            clearFocusCalled = true
        }
    }
    
    internal class MockUpdateTimer: UpdateTimerInterface {
        internal var updateHandler: (() -> Void)?
        internal var startUpdatesCalled = false
        internal var stopUpdatesCalled = false
        
        internal func startUpdates(handler: @escaping () -> Void) {
            startUpdatesCalled = true
            updateHandler = handler
        }
        
        internal func stopUpdates() {
            stopUpdatesCalled = true
        }
    }
}
