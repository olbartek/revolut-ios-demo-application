import UIKit
import Foundation

extension CurrenciesConverterViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView,
                          didSelectRowAt indexPath: IndexPath) {
        eventHandler.handleDidSelectRow(atIndex: indexPath.row)
    }
    
    public func tableView(_ tableView: UITableView,
                          heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        eventHandler.handleWillStartDraggingTableView()
    }
}

extension CurrenciesConverterViewController: UITextFieldDelegate {
    public func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        guard let currentString = textField.text, let range = Range(range, in: currentString) else {
            preconditionFailure()
        }
        
        let newString = currentString.replacingCharacters(in: range, with: string)
        
        return eventHandler.handleTextFieldTextShouldChange(to: newString)
    }
}
