import UIKit

public class CurrencyTableViewCell: UITableViewCell, Reusable {
    private struct Constants {
        static let imageViewCornerRadius: CGFloat = 20.0
    }
    
    public let currencyFlagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        imageView.layer.borderWidth = 1.0
        imageView.layer.cornerRadius = Constants.imageViewCornerRadius
        imageView.layer.masksToBounds = true
        
        return imageView
    }()
    
    public let currencyCodeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.setContentCompressionResistancePriority(.required, for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    public let currencyNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    public let currencyAmountTextField: UITextField = {
        let textField = UITextField()
        textField.keyboardType = .decimalPad
        textField.font = UIFont.systemFont(ofSize: 28.0)
        textField.textAlignment = .right
        textField.placeholder = "0"
        textField.setContentHuggingPriority(.required, for: .horizontal)
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        return textField
    }()
    
    public let currencyAmountUnderlineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private let currencyCodeAndNameStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 16.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    }()
    
    // MARK: - Init
    override public init(style: UITableViewCell.CellStyle,
                         reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        setupView()
        installConstraints()
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View setup
    private func setupView() {
        [currencyCodeLabel, currencyNameLabel]
            .forEach(currencyCodeAndNameStackView.addArrangedSubview)
        [currencyFlagImageView,
         currencyCodeAndNameStackView,
         currencyAmountTextField]
            .forEach(contentStackView.addArrangedSubview)
        contentView.addSubview(contentStackView)
        contentView.addSubview(currencyAmountUnderlineView)
    }
    
    private func installConstraints() {
        NSLayoutConstraint.activate([
            currencyFlagImageView.widthAnchor
                .constraint(equalTo: currencyFlagImageView.heightAnchor),
            contentStackView.leadingAnchor
                .constraint(equalTo: contentView.leadingAnchor),
            contentStackView.trailingAnchor
                .constraint(equalTo: contentView.trailingAnchor),
            contentStackView.centerYAnchor
                .constraint(equalTo: contentView.centerYAnchor),
            contentStackView.heightAnchor
                .constraint(equalTo: contentView.heightAnchor, multiplier: 0.45),
            currencyAmountUnderlineView.heightAnchor
                .constraint(equalToConstant: 1.0),
            currencyAmountUnderlineView.topAnchor
                .constraint(equalTo: currencyAmountTextField.bottomAnchor),
            currencyAmountUnderlineView.leadingAnchor
                .constraint(equalTo: currencyAmountTextField.leadingAnchor),
            currencyAmountUnderlineView.trailingAnchor
                .constraint(equalTo: currencyAmountTextField.trailingAnchor)
        ])
    }
}
