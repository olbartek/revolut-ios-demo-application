import UIKit

extension CurrenciesConverterViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView,
                          numberOfRowsInSection section: Int) -> Int {
        return presenter.viewModel.currencies.count
    }
    
    public func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        let currencyViewModel = presenter.viewModel.currencies[indexPath.row]
        cell.bind(to: currencyViewModel)
        
        if indexPath.row == 0 {
            cell.currencyAmountTextField.delegate = self
        } else {
            cell.currencyAmountTextField.delegate = nil
        }
        
        return cell
    }
}
