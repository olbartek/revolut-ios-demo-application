import DeepDiff
import UIKit

public final class CurrenciesConverterViewController: UIViewController {
    internal struct Constants {
        static let rowHeight: CGFloat = 90.0
    }
    
    // MARK: - Properties
    public var presenter: CurrenciesConverterPresentation!
    public var eventHandler: CurrenciesConverterEventHandler!
    
    public let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()

    // MARK: - Lifecycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        eventHandler.handleViewReady()
    }

    // MARK: - Private Methods
    private func setupView() {
        view.backgroundColor = .white
        
        tableView.register(cellType: CurrencyTableViewCell.self)
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        instalConstraints()
    }
    
    private func instalConstraints() {
        NSLayoutConstraint.activate([
            view.safeAreaLayoutGuide.topAnchor
                .constraint(equalTo: tableView.topAnchor),
            view.layoutMarginsGuide.leadingAnchor
                .constraint(equalTo: tableView.leadingAnchor),
            view.safeAreaLayoutGuide.bottomAnchor
                .constraint(equalTo: tableView.bottomAnchor),
            view.layoutMarginsGuide.trailingAnchor
                .constraint(equalTo: tableView.trailingAnchor)
        ])
    }
}

extension CurrenciesConverterViewController: CurrenciesConverterView {
    public func reload(changes: [Change<CurrencyViewModel>], updateData: () -> Void) {
        tableView.reload(changes: changes,
                         insertionAnimation: .none,
                         deletionAnimation: .none,
                         replacementAnimation: .none,
                         updateData: updateData)
    }
    
    public func reload(rows: [Int]) {
        tableView.reloadRows(at: rows.map{ IndexPath(row: $0, section: 0) }, with: .none)
    }
    
    public func scrollToFirstRowIfNeeded() {
        let firstRowIndexPath = IndexPath(row: 0, section: 0)
        if let visibleRows = tableView.indexPathsForVisibleRows,
            !visibleRows.contains(firstRowIndexPath) {
            tableView.scrollToRow(at: firstRowIndexPath, at: .top, animated: true)
        }
    }
    
    public func clearFocus() {
        view.endEditing(true)
    }
}
