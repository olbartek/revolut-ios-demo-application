import UIKit

extension CurrencyTableViewCell {
    public func bind(to viewModel: CurrencyViewModel) {
        currencyFlagImageView.image = UIImage(named: viewModel.assetName)
        currencyCodeLabel.text = viewModel.code
        currencyNameLabel.text = viewModel.name
        currencyAmountTextField.text = viewModel.amount
        currencyAmountTextField.isUserInteractionEnabled = viewModel.isFocused
        
        if viewModel.isFocused {
            currencyAmountTextField.becomeFirstResponder()
        }
        
        let underlineColor = viewModel.isFocused ? UIColor.blue : UIColor.gray.withAlphaComponent(0.5)
        currencyAmountUnderlineView.backgroundColor = underlineColor
    }
}

private extension CurrencyViewModel {
    var assetName: String {
        return code.uppercased()
    }
}
