public struct Currency: Equatable, Hashable {
    public let code: CurrencyCode
    public let name: String
    
    public init(code: CurrencyCode, name: String) {
        self.code = code
        self.name = name
    }
}
