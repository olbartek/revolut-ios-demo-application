import Foundation

public struct CurrencyRate: Equatable, Hashable {
    public let fromCurrency: Currency
    public let toCurrency: Currency
    public let rate: Decimal
    
    public init(fromCurrency: Currency,
                toCurrency: Currency,
                rate: Decimal) {
        self.fromCurrency = fromCurrency
        self.toCurrency = toCurrency
        self.rate = rate
    }
}
