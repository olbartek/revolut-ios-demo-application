import Foundation

internal extension CurrencyRate {
    static func stub(fromCurrency: Currency = .stub(),
                     toCurrency: Currency = .stub(),
                     rate: Decimal = 1.0) -> CurrencyRate {
        return CurrencyRate(fromCurrency: fromCurrency,
                            toCurrency: toCurrency,
                            rate: rate)
    }
}
