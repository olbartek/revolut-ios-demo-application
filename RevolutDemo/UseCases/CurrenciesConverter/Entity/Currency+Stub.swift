internal extension Currency {
    static func stub(code: String = "fixture",
                     name: String = "fixture") -> Currency {
        return Currency(code: code, name: name)
    }
}
