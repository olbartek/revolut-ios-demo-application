import Foundation

public protocol CurrenciesConverterUseCase: class {
    func fetchCurrenciesRates(baseCurrencyCode: CurrencyCode)
    func cancelFetchingCurrenciesRates()
}

public protocol CurrenciesConverterUseCaseOutput: class {
    func didFetchCurrenciesRates(response: [CurrencyRate])
    func didFailFetchingCurrenciesRates(error: Error)
}

public protocol CurrenciesConverterEntityGateway: class {
    func getCurrenciesRates(baseCurrencyCode: CurrencyCode,
                            success: @escaping ([CurrencyRate]) -> (),
                            failure: @escaping (Error) -> ()) -> Cancellable
}
