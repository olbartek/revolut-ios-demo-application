@testable import RevolutDemo
import XCTest

extension CurrenciesConverterInteractorTests {
    internal func testInteractorCallsDataManagerOnFetchCurrenciesRates() {
        // Act
        interactor.fetchCurrenciesRates(baseCurrencyCode: "fixture")
        
        // Assert
        XCTAssertTrue(dataManager.getCurrenciesRatesCalled)
    }
    
    internal func testInteractorCallsOutputOnFetchSuccess() {
        // Arrange
        dataManager.shouldFail = false
        
        // Act
        interactor.fetchCurrenciesRates(baseCurrencyCode: "fixture")
        
        // Assert
        XCTAssertNotNil(output.fetchCurrenciesResponse)
    }
    
    internal func testInteractorCallsOutputOnFetchFailure() {
        // Arrange
        dataManager.shouldFail = true
        
        // Act
        interactor.fetchCurrenciesRates(baseCurrencyCode: "fixture")
        
        // Assert
        XCTAssertNotNil(output.fetchCurrenciesError)
    }
}
