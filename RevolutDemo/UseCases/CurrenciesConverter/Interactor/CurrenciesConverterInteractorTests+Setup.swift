@testable import RevolutDemo
import XCTest

internal class CurrenciesConverterInteractorTests: XCTestCase {
    internal var interactor: CurrenciesConverterInteractor!
    internal var output: MockOutput!
    internal var dataManager: MockDataManager!
    
    override internal func setUp() {
        super.setUp()
        
        interactor = CurrenciesConverterInteractor()
        output = MockOutput()
        dataManager = MockDataManager()
        
        interactor.output = output
        interactor.dataManager = dataManager
    }
    
    override internal func tearDown() {
        interactor = nil
        output = nil
        dataManager = nil
        
        super.tearDown()
    }
    
    // MARK: - Mocks
    internal class MockOutput: CurrenciesConverterUseCaseOutput {
        internal var fetchCurrenciesResponse: [CurrencyRate]?
        internal var fetchCurrenciesError: Error?
        
        internal func didFetchCurrenciesRates(response: [CurrencyRate]) {
            fetchCurrenciesResponse = response
        }
        
        internal func didFailFetchingCurrenciesRates(error: Error) {
            fetchCurrenciesError = error
        }
    }
    
    internal class MockDataManager: CurrenciesConverterEntityGateway {
        internal var getCurrenciesRatesCalled = false
        internal var shouldFail = false
        
        internal func getCurrenciesRates(baseCurrencyCode: CurrencyCode,
                                         success: @escaping ([CurrencyRate]) -> (),
                                         failure: @escaping (Error) -> ()) -> Cancellable {
            getCurrenciesRatesCalled = true
            if shouldFail {
                failure(MockError.fixture)
            } else {
                success([
                    CurrencyRate(
                        fromCurrency: .stub(code: "EUR", name: "Euro"),
                        toCurrency: .stub(code: "PLN", name: "Polish Zloty"),
                        rate: 4.23)
                    ])
            }
            
            return EmptyCancellable()
        }
    }
}

private struct EmptyCancellable: Cancellable {
    func cancel() {
    }
}

private enum MockError: Error {
    case fixture
}
