import Foundation

public final class CurrenciesConverterInteractor {
    // MARK: - Properties
    public weak var output: CurrenciesConverterUseCaseOutput!
    public var dataManager: CurrenciesConverterEntityGateway!
    
    private var currentFetchCurrenciesRatesTask: Cancellable?

    // MARK: - Initializers
    public init() {}
    
    deinit {
    }
}

extension CurrenciesConverterInteractor: CurrenciesConverterUseCase {
    public func fetchCurrenciesRates(baseCurrencyCode: CurrencyCode) {
        currentFetchCurrenciesRatesTask = dataManager
            .getCurrenciesRates(baseCurrencyCode: baseCurrencyCode,
                                success: { [weak output] currenciesRates in
                                        output?.didFetchCurrenciesRates(response: currenciesRates)
                                }, failure: { [weak output] error in
                                        output?.didFailFetchingCurrenciesRates(error: error)
                                })
    }
    
    public func cancelFetchingCurrenciesRates() {
        currentFetchCurrenciesRatesTask?.cancel()
    }
}
