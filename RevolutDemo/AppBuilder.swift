import UIKit

internal class AppBuilder {
    private let repository: AppRepository
    
    internal init(repository: AppRepository) {
        self.repository = repository
    }
}

extension AppBuilder: CurrenciesConverterBuilder {
    internal func buildCurrenciesConverter() -> UIViewController {
        let interactor = CurrenciesConverterInteractor()
        let presenter = CurrenciesConverterPresenter()
        let viewController = CurrenciesConverterViewController()
        
        interactor.dataManager = repository
        presenter.interactor = interactor
        presenter.view = viewController
        interactor.output = presenter
        viewController.presenter = presenter
        viewController.eventHandler = presenter
        
        return viewController
    }
}
