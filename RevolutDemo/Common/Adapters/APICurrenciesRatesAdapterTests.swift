@testable import RevolutDemo
import XCTest

internal class APICurrencyRatesAdapterTests: XCTestCase {
    internal var sut: APICurrencyRatesAdapter!
    
    override internal func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    internal func testCurrenciesRatesConversion() throws {
        // Arrange
        let response = CurrenciesRates.Response(base: "EUR",
                                                rates: ["USD": 1.234, "GBP": 3.456])
        sut = APICurrencyRatesAdapter(apiResponse: response)
        
        // Act
        let result = try sut.toCurrencyRates()
        
        // Assert
        let baseCurrencyRate = CurrencyRate(fromCurrency: eurCurrency(),
                                            toCurrency: eurCurrency(),
                                            rate: 1.0)
        let usdCurrencyRate = CurrencyRate(fromCurrency: eurCurrency(),
                                           toCurrency: usdCurrency(),
                                           rate: 1.234)
        let gbpCurrencyRate = CurrencyRate(fromCurrency: eurCurrency(),
                                           toCurrency: gbpCurrency(),
                                           rate: 3.456)
        
        XCTAssertEqual(result[0], baseCurrencyRate)
        XCTAssertTrue(result.contains(usdCurrencyRate))
        XCTAssertTrue(result.contains(gbpCurrencyRate))
    }
    
    private func gbpCurrency() -> Currency {
        return Currency(code: "GBP", name: "British Pound Sterling")
    }
    
    private func eurCurrency() -> Currency {
        return Currency(code: "EUR", name: "Euro")
    }
    
    private func usdCurrency() -> Currency {
        return Currency(code: "USD", name: "US Dollar")
    }
}
