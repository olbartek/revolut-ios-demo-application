import Foundation

internal class APICurrencyRatesAdapter {
    private let apiResponse: CurrenciesRates.Response
    private let currenciesStorage = CurrenciesInfoStorage()
    
    internal init(apiResponse: CurrenciesRates.Response) {
        self.apiResponse = apiResponse
    }
    
    internal func toCurrencyRates() throws -> [CurrencyRate] {
        let (baseCurrency, baseCurrencyRate) = try createBaseCurrency()
        
        var currencyRates = try apiResponse.rates.enumerated()
            .map { (_, element) -> CurrencyRate in
                let code = element.key
                let rate = element.value
                guard let name = currenciesStorage
                    .name(forCurrencyCode: code) else {
                        throw Error.unknownCurrencyCode
                }
                
                let currency = Currency(code: code, name: name)
                
                return CurrencyRate(
                    fromCurrency: baseCurrency,
                    toCurrency: currency,
                    rate: rate
                )
        }
        currencyRates.insert(baseCurrencyRate, at: 0)
        
        return currencyRates
    }
    
    private func createBaseCurrency() throws -> (Currency, CurrencyRate) {
        let baseCurrencyCode = apiResponse.base
        guard let baseCurrencyName = currenciesStorage
            .name(forCurrencyCode: baseCurrencyCode) else {
                throw Error.unknownCurrencyCode
        }
        
        let baseCurrency = Currency(code: baseCurrencyCode,
                                    name: baseCurrencyName)
        let baseCurrencyRate = CurrencyRate(fromCurrency: baseCurrency,
                                            toCurrency: baseCurrency,
                                            rate: 1.0)
        
        return (baseCurrency, baseCurrencyRate)
    }
}

extension APICurrencyRatesAdapter {
    internal enum Error: Swift.Error {
        case unknownCurrencyCode
    }
}

// INFO: This is just a helper class which takes currencies info from `currencies.json` file.
//       (a json file consists of currencies list according to ISO 4217 standard)
//       The class is used as a helper to retrieve a name for each currency.
//       Normally this kind of information would probably be returned from the server.
private class CurrenciesInfoStorage {
    private struct CurrencyInfo: Decodable {
        let name: String
        let code: String
    }

    private static var currenciesInfo: [CurrencyCode: CurrencyInfo] {
        let url = Bundle.main.url(forResource: "currencies", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let decoder = JSONDecoder()
        let jsonData = try! decoder.decode([CurrencyCode: CurrencyInfo].self,
                                           from: data)
        
        return jsonData
    }
    
    func name(forCurrencyCode currencyCode: CurrencyCode) -> String? {
        return CurrenciesInfoStorage.currenciesInfo[currencyCode]?.name
    }
}
