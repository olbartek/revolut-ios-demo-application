import Foundation

public protocol DecimalInputAllowing {
    var locale: Locale { get }
    var maximumNumberOfFractionalPartDigits: Int { get }
    var maximumNumberOfIntegralPartDigits: Int { get }
    
    func shouldAllow(decimalInput: String, currentDecimalString: String) -> Bool
}

extension DecimalInputAllowing {
    public func shouldAllow(decimalInput: String, currentDecimalString: String) -> Bool {
        let decimalSeparator = locale.decimalSeparator ?? "."
        let groupingSeparator = locale.groupingSeparator ?? ""
        let decimalComponents = decimalInput.split(separator: Character(decimalSeparator),
                                                   omittingEmptySubsequences: false)
        let isDeletingCharacters = currentDecimalString.count > decimalInput.count
        
        let computeNumberOfIntegralPartDigits: () -> Int = {
            return decimalComponents.first!
                .replacingOccurrences(of: groupingSeparator, with: "")
                .count
        }
        let computeNumberOfFractionalPartDigits: () -> Int = {
            return decimalComponents.last!.count
        }
        
        let numberOfIntegralPartDigits: Int
        let numberOfFractionalPartDigits: Int
        
        switch decimalComponents.count {
        case 0...1:
            numberOfIntegralPartDigits = computeNumberOfIntegralPartDigits()
            numberOfFractionalPartDigits = 0
        case 2:
            numberOfIntegralPartDigits = computeNumberOfIntegralPartDigits()
            numberOfFractionalPartDigits = computeNumberOfFractionalPartDigits()
        default:
            return false
        }
        
        guard !isDeletingCharacters else {
            return true
        }
        
        guard numberOfFractionalPartDigits <= maximumNumberOfFractionalPartDigits else {
            return false
        }
        
        guard numberOfIntegralPartDigits <= maximumNumberOfIntegralPartDigits else {
            return false
        }
        
        return true
    }
}
