import Foundation

public protocol DecimalAmountConverting {
    var locale: Locale { get }
    
    func convertToStringAmount(value: Decimal) -> String
    func convertToDecimalAmount(value: String) -> Decimal?
}

extension DecimalAmountConverting {
    public func convertToStringAmount(value: Decimal) -> String {
        let groupingSeparator = locale.groupingSeparator ?? ""
        return String(format: "%.2f", locale: locale, value.doubleValue)
            .replacingOccurrences(of: groupingSeparator, with: "")
    }
    
    public func convertToDecimalAmount(value: String) -> Decimal? {
        return Decimal(string: value, locale: locale)
    }
}
