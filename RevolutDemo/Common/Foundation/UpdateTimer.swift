import Foundation

public protocol UpdateTimerInterface {
    var updateHandler: (() -> Void)? { get }
    
    func startUpdates(handler: @escaping () -> Void)
    func stopUpdates()
}

public class UpdateTimer: UpdateTimerInterface {
    private var timer: Timer?
    private let timeInterval: TimeInterval
    public var updateHandler: (() -> Void)?
    
    public init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }
    
    public func startUpdates(handler: @escaping () -> Void) {
        updateHandler = handler
        timer = Timer.scheduledTimer(
            withTimeInterval: timeInterval,
            repeats: true,
            block: { [weak self] _ in
                self?.updateHandler?()
        })
        timer?.fire()
    }
    
    public func stopUpdates() {
        timer?.invalidate()
        timer = nil
    }
}
