public extension String {
    // INFO: This is needed to be sure that we have valid unsigned int number
    //       As code `UInt("0101")` produces non-nil result, this method ensures
    //       that always a valid number is processed (without leading zeros)
    func toUnsignedIntegerStringIfPossible() -> String {
        guard let unsignedInteger = UInt(self) else {
            return self
        }
        
        return String(describing: unsignedInteger)
    }
    
    func zeroStringIfEmpty() -> String {
        return isEmpty ? "0" : self
    }
}
