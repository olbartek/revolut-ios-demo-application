@testable import RevolutDemo
import XCTest

internal class DecimalInputAllowingTests: XCTestCase {
    internal var sut: TestableImplementation!
    
    override internal func setUp() {
        super.setUp()
        
        sut = TestableImplementation()
    }
    
    override internal func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    internal func testNumberOfDecimalsAllowanceWhenTheLimitIsSetToTwoDecimals() {
        // Arrange
        sut.maximumNumberOfFractionalPartDigits = 2
        
        // Act & Assert
        // when adding characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "0", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12", currentDecimalString: "1"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.3", currentDecimalString: "12."))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.34", currentDecimalString: "12.3"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "12.345", currentDecimalString: "12.34"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "12.3456", currentDecimalString: "12.345"))
        // when removing characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.345", currentDecimalString: "12.3456"))
    }
    
    internal func testNumberOfDecimalsAllowanceWhenTheLimitIsSetToFiveDecimals() {
        // Arrange
        sut.maximumNumberOfFractionalPartDigits = 5
        
        // Act & Assert
        // when adding characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "0", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12", currentDecimalString: "1"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.3", currentDecimalString: "12"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.34", currentDecimalString: "12.3"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.345", currentDecimalString: "12.34"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.3456", currentDecimalString: "12.345"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.34567", currentDecimalString: "12.3456"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "12.345678", currentDecimalString: "12.34567"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "12.3456789", currentDecimalString: "12.345678"))
        // when removing characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.345678", currentDecimalString: "12.3456789"))
    }
    
    internal func testNumberOfIntegralPartDigitsAllowanceWhenTheLimitIsSetToNine() {
        // Arrange
        sut.maximumNumberOfIntegralPartDigits = 9
        
        // Act & Assert
        // when adding characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "0", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.0", currentDecimalString: "12"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "123.0", currentDecimalString: "123"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "1,234.0", currentDecimalString: "1,234"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12,345.0", currentDecimalString: "12,345"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "123,456.0", currentDecimalString: "123,456"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "1,234,567.0", currentDecimalString: "1,234,567"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12,345,678.0", currentDecimalString: "12,345,678"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "123,456,789.0", currentDecimalString: "123,456,789"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "10,123,456,789.0", currentDecimalString: "10,123,456,789"))
        // when removing characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "10,123,456,789.0", currentDecimalString: "10,123,456,789.00"))
    }
    
    internal func testNumberOfIntegralPartDigitsAllowanceWhenTheLimitIsSetToFive() {
        // Arrange
        sut.maximumNumberOfIntegralPartDigits = 5
        
        // Act & Assert
        // when adding characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "0", currentDecimalString: ""))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12.0", currentDecimalString: "12"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "123.0", currentDecimalString: "123"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "1,234.0", currentDecimalString: "1,234"))
        XCTAssertTrue(sut.shouldAllow(decimalInput: "12,345.0", currentDecimalString: "12,345"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "123,456.0", currentDecimalString: "123,456"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "1,234,567.0", currentDecimalString: "1,234,567"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "12,345,678.0", currentDecimalString: "12,345,678"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "123,456,789.0", currentDecimalString: "123,456,789"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "10,123,456,789.0", currentDecimalString: "10,123,456,789"))
        // when removing characters
        XCTAssertTrue(sut.shouldAllow(decimalInput: "10,123,456,789.0", currentDecimalString: "10,123,456,789.00"))
    }
    
    internal func testUnknownNumberIsNotAllowed() {
        // Act & Assert
        XCTAssertFalse(sut.shouldAllow(decimalInput: "123.456.789", currentDecimalString: "123.456.7"))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "123....7", currentDecimalString: ""))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "1....6789", currentDecimalString: ""))
        XCTAssertFalse(sut.shouldAllow(decimalInput: "1.0.0.0.7", currentDecimalString: ""))
    }

}

extension DecimalInputAllowingTests {
    internal class TestableImplementation: DecimalInputAllowing {
        internal var locale = Locale(identifier: "en_US")
        internal var maximumNumberOfFractionalPartDigits: Int = 2
        internal var maximumNumberOfIntegralPartDigits: Int = 9
    }
}
