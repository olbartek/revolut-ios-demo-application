@testable import RevolutDemo
import XCTest

internal class DecimalAmountConvertingTests: XCTestCase {
    internal var sut: TestableImplementation!
    
    override internal func setUp() {
        super.setUp()
        
        sut = TestableImplementation()
    }
    
    override internal func tearDown() {
        sut = nil
        
        super.tearDown()
    }
    
    internal func testZeroValueConversionForLocalesWithCommaDecimalSeparator() {
        // Arrange
        sut.locale = .init(identifier: "pl_PL")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 0.0), "0,00")
    }
    
    internal func testZeroValueConversionForLocalesWithDotDecimalSeparator() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 0.0), "0.00")
    }
    
    internal func testDecimalValueConversionForLocalesWithCommaDecimalSeparator() {
        // Arrange
        sut.locale = .init(identifier: "pl_PL")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 123.454234), "123,45")
        XCTAssertEqual(sut.convertToStringAmount(value: 55), "55,00")
        XCTAssertEqual(sut.convertToStringAmount(value: 13.5), "13,50")
    }
    
    internal func testDecimalValueConversionForLocalesWithDotDecimalSeparator() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 23.454234), "23.45")
        XCTAssertEqual(sut.convertToStringAmount(value: 100), "100.00")
        XCTAssertEqual(sut.convertToStringAmount(value: 22.6), "22.60")
    }
    
    internal func testGroupingSeparatorIsRemovedFromResultedStringForLocalesWithCommaGroupingSeparator() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 123_456.789), "123456.79") // Normally 123,456.79
    }
    
    internal func testGroupingSeparatorIsRemovedFromResultedStringForLocalesWithDotGroupingSeparator() {
        // Arrange
        sut.locale = .init(identifier: "fr_MA")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 123_456.789), "123456,79") // Normally 123.456,79
    }
    
    internal func testGroupingSeparatorIsRemovedFromResultedStringForLocalesWithSpaceGroupingSeparator() {
        // Arrange
        sut.locale = .init(identifier: "uk_UA")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToStringAmount(value: 123_456.789), "123456,79") // Normally 123 456,79
    }
    
    internal func testZeroStringToDecimalConversion() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToDecimalAmount(value: "0"), 0)
        XCTAssertEqual(sut.convertToDecimalAmount(value: "0.0"), 0)
        XCTAssertEqual(sut.convertToDecimalAmount(value: "0."), 0)
        XCTAssertEqual(sut.convertToDecimalAmount(value: ".0"), 0)
    }
    
    internal func testDecimalStringToDecimalConversion() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertEqual(sut.convertToDecimalAmount(value: "30"), 30)
        XCTAssertEqual(sut.convertToDecimalAmount(value: "1234.55"), 1234.55)
        XCTAssertEqual(sut.convertToDecimalAmount(value: "45.4511"), 45.4511)
        XCTAssertEqual(sut.convertToDecimalAmount(value: "53.5245234"), 53.5245234)
    }
    
    internal func testDecimalStringWithWrongDecimalSeparatorIsNotConvertedToDecimalForSomeCases() {
        // Arrange
        sut.locale = .init(identifier: "en_US")
        
        // Act & Assert
        XCTAssertNil(sut.convertToDecimalAmount(value: ",0"))
    }
    
    internal func testNonDecimalStringIsNotConvertedToDecimal() {
        // Act & Assert
        XCTAssertNil(sut.convertToDecimalAmount(value: "hello"))
    }
}

extension DecimalAmountConvertingTests {
    internal class TestableImplementation: DecimalAmountConverting {
        internal var locale = Locale(identifier: "pl-PL")
    }
}
