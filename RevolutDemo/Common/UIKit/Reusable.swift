import UIKit

public protocol Reusable {
    static var reuseID: String { get }
}

public extension Reusable where Self: UITableViewCell {
    static var reuseID: String {
        return String(describing: self)
    }
}

public extension UITableView {
    func dequeueReusableCell<T: UITableViewCell & Reusable>(for indexPath: IndexPath) -> T {
        let cell: T! = dequeueReusableCell(withIdentifier: T.reuseID, for: indexPath) as? T
        return cell
    }
}

public extension UITableView {
    func register<T: UITableViewCell & Reusable>(cellType: T.Type) {
        register(cellType, forCellReuseIdentifier: T.reuseID)
    }
}
